package com.web.book.springboot.web;

import org.junit.Test;
import org.springframework.mock.env.MockEnvironment;

import static org.assertj.core.api.Assertions.assertThat;


public class ProfileControllerUintTest {

    @Test
    public void real_profile_will_be_selected(){
        //given
        String expectedProfile = "real";
        MockEnvironment env =  new MockEnvironment();
        env.addActiveProfile(expectedProfile);
        env.addActiveProfile("oath");
        env.addActiveProfile("real-db");

        ProfileController controller = new ProfileController(env);

        // when
        String profile = controller.profile();

        //then
        assertThat(profile).isEqualTo(expectedProfile);
    }

    @Test
    public void real_profile_will_be_first_selected(){
        //given
        String expectedProfile = "oath";
        MockEnvironment env =  new MockEnvironment();
        env.addActiveProfile(expectedProfile);
        env.addActiveProfile("real-db");

        ProfileController controller = new ProfileController(env);

        // when
        String profile = controller.profile();

        //then
        assertThat(profile).isEqualTo(expectedProfile);
    }

    @Test
    public void active_profile_will_be_first_selected(){
        //given
        String expectedProfile = "default";
        MockEnvironment env =  new MockEnvironment();

        ProfileController controller = new ProfileController(env);

        // when
        String profile = controller.profile();

        //then
        assertThat(profile).isEqualTo(expectedProfile);
    }

}
